# Copyright (C) 2004, 2005, 2008 Patrick Pelissier, Paul Zimmermann,
# LORIA/INRIA Lorraine.
# 
# This file is part of the DPE Library.
# 
# The DPE Library is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at your
# option) any later version.
# 
# The DPE Library is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with the DPE Library; see the file COPYING.LIB.
# If not, see <https://www.gnu.org/licenses/>.
# 
CC=gcc
CFLAGS=-g -W -Wall -Wmissing-prototypes -O3 -fomit-frame-pointer
RM=rm -f

FILES=Makefile dpe.h test.c COPYING.LIB README NEWS
# if you change the version, please change it in dpe.h too
VERSION=1.6

# add -std=c99 to avoid the warnings about round(), trunc(), isfinite, ...
test: test.c dpe.h
	$(CC) $(CFLAGS) -I$(GMP)/include -L$(GMP)/lib $< -o $@ -lgmp -lm

test-long: test.c dpe.h
	$(CC) $(CFLAGS) -I$(GMP)/include -L$(GMP)/lib -DDPE_USE_LONG $< -o $@ -lgmp -lm

test-longlong: test.c dpe.h
	$(CC) $(CFLAGS) -I$(GMP)/include -L$(GMP)/lib -DDPE_USE_LONGLONG $< -o $@ -lgmp -lm

test-ldouble: test.c dpe.h
	$(CC) $(CFLAGS) -I$(GMP)/include -L$(GMP)/lib -DDPE_USE_LONGDOUBLE $< -o $@ -lgmp -lm

test-float128: test.c dpe.h
	$(CC) $(CFLAGS) -I$(GMP)/include -L$(GMP)/lib -DDPE_USE_FLOAT128 $< -o $@ -lgmp -lm -lquadmath

check128: test-float128
	./test-float128

check: test test-ldouble test-long test-longlong
	./test
	./test-ldouble
	./test-long
	./test-longlong

clean:
	$(RM) *.o test test-ldouble test-long test-longlong test-float128

dist: $(FILES)
	mkdir dpe-$(VERSION)
	cp $(FILES) dpe-$(VERSION)
	tar cf dpe-$(VERSION).tar dpe-$(VERSION)
	gzip --best dpe-$(VERSION).tar
	/bin/rm -fr dpe

