/* Copyright (C) 2004-2024 Patrick Pelissier, Paul Zimmermann, LORIA/INRIA.

This file is part of the DPE Library.

The DPE Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The DPE Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the DPE Library; see the file COPYING.LIB.
If not, see <https://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <stdlib.h>

#include "gmp.h"
#include "dpe.h"

#if defined(__i386__) || defined(__i386)
# undef x86
# define x86
#endif

int
main ()
{
  dpe_t x, y;
  mpz_t z;
  DPE_DOUBLE d;
  int bitsize;
  long l;
  mp_exp_t e;

#if defined(DPE_USE_DOUBLE)
  printf ("Test DPE with mantissa = double and exponent = " );
#elif defined(DPE_USE_LONGDOUBLE)
  printf ("Test DPE with long double and exponent = " );
#elif defined(DPE_USE_FLOAT128)
  printf ("Test DPE with __float128 and exponent = " );
#endif

#if defined(DPE_USE_LONG)
  printf ("long.\n");
#elif defined(DPE_USE_LONGLONG)
  printf ("long long.\n");
#else
  printf ("int.\n");
#endif

  dpe_init (x);
  dpe_init (y);
  mpz_init (z);

#ifdef x86
#define setfpucw(cw) do { unsigned int x = (cw); __asm__ ("fldcw %0" : : "m" (x)); } while (0)
#define _FPU_DEFAULT  0x137f
#define _FPU_EXTENDED 0x300
#define _FPU_DOUBLE   0x200
#define _fpu_ieee ((_FPU_DEFAULT & (~_FPU_EXTENDED)) | _FPU_DOUBLE)
  setfpucw(_fpu_ieee);
#endif

  /* check DPE_BITSIZE */
  d = 1.0;
  for (bitsize = 0; d + 1.0 != d; d *= 2.0, bitsize ++);
  if (bitsize != DPE_BITSIZE)
    {
      fprintf (stderr, "Error, bit-size of DPE_DOUBLE type does not match: declared %u, found %d\n", DPE_BITSIZE, bitsize);
      exit (1);
    }

  /* test set */
  dpe_set_ui (x, 17);
  dpe_set_ui (y, 0);
  dpe_set (y, x);
  if (dpe_cmp_ui (y, 17))
    printf ("dpe_set failed!\n"), abort ();

  /* test neg */
  dpe_neg (y, y);
  if (dpe_cmp_si (y, -17))
    printf ("dpe_neg failed!\n"), abort ();

  /* test abs */
  dpe_abs (y, y);
  if (dpe_cmp_ui (y, 17))
    printf ("dpe_abs failed!\n"), abort ();

  /* test normalize */
  DPE_MANT(x) = -21.0;
  DPE_EXP(x) = 1;
  dpe_normalize (x);
  if (dpe_cmp_si (x, -42))
    printf ("dpe_normalize failed!\n"), abort ();

  /* test set_d */
  dpe_set_d (x, 42.0);
  if (dpe_cmp_d (x, 42.0))
    printf ("dpe_set_d failed!\n"), abort ();

  /* test set_ld */
  dpe_set_ld (x, (long double) 42.0);
  if (dpe_cmp_d (x, 42.0))
    printf ("dpe_set_ld failed!\n"), abort ();

  /* test set_ui */
  dpe_set_ui (x, 17);
  if (dpe_cmp_ui (x, 17))
    printf ("dpe_cmp_ui (x, 17) failed!\n"), abort ();
  
  /* test set_si */
  dpe_set_si (x, -42);
  if (dpe_cmp_si (x, -42))
    printf ("dpe_set_si failed!\n"), abort ();

  /* test get_si */
  if (dpe_get_si (x) != -42)
    printf ("dpe_get_si failed!\n"), abort ();

  /* test get_ui */
  dpe_set_ui (x, 42);
  if (dpe_get_ui (x) != 42)
    printf ("dpe_get_ui failed!\n"), abort ();

  /* test get_d */
  dpe_set_ui (x, 42);
  if (dpe_get_d (x) != 42.0)
    printf ("dpe_get_d failed!\n"), abort ();

  /* test get_ld */
  dpe_set_ui (x, 42);
  if (dpe_get_ld (x) != (long double) 42.0)
    printf ("dpe_get_ld failed!\n"), abort ();

  /* test set_z */
  mpz_set_si (z, -17);
  dpe_set_z (x, z);
  if (dpe_cmp_si (x, -17))
    printf ("dpe_set_z failed!\n"), abort ();

  /* test get_z */
  dpe_set_si (x, -42);
  dpe_get_z (z, x);
  if (mpz_cmp_si (z, -42))
    printf ("dpe_get_z failed!\n"), abort ();

  dpe_set_d (x, 9007199254740992.0); /* 2^53 */
  dpe_get_z (z, x);
  if (mpz_get_d (z) != 9007199254740992.0)
    printf ("dpe_get_z failed (2)!\n"), abort ();

  /* test get_z_exp */
  dpe_set_si (x, -42);
  e = dpe_get_z_exp (z, x);
  if (e >= 0)
    mpz_mul_2exp (z, z, e);
  else
    mpz_div_2exp (z, z, -e);
  if (mpz_cmp_si (z, -42))
    printf ("dpe_get_z_exp failed!\n"), abort ();

  /* test add */
  dpe_set_ui (x, 17);
  dpe_set_ui (y, 42);
  dpe_add (x, x, y);
  if (dpe_cmp_ui (x, 59))
    printf ("dpe_add failed!\n"), abort ();

  /* test sub */
  dpe_set_ui (x, 17);
  dpe_set_ui (y, 42);
  dpe_sub (x, x, y);
  if (dpe_cmp_si (x, -25))
    printf ("dpe_sub failed!\n"), abort ();
  
  /* test mul */
  dpe_set_ui (x, 17);
  dpe_set_ui (y, 42);
  dpe_mul (x, x, y);
  if (dpe_cmp_ui (x, 714))
    printf ("dpe_mul failed!\n"), abort ();

  /* dpe_sqrt */
  dpe_set_d (x, 1.0);
  dpe_sqrt (x, x);
  if (dpe_cmp_ui (x, 1))
    abort ();
  dpe_set_d (x, 2.0);
  dpe_sqrt (x, x);
  if (dpe_cmp_d (x, sqrt (2.0)))
    abort ();
  dpe_set_d (x, 0.5);
  dpe_sqrt (x, x);
  if (dpe_cmp_d (x, sqrt (0.5)))
    abort ();
  dpe_set_d (x, 0.25);
  dpe_sqrt (x, x);
  if (dpe_cmp_d (x, 0.5))
    abort ();

  /* test div */
  dpe_set_ui (x, 17);
  dpe_set_ui (y, 42);
  dpe_div (x, x, y);
  if (dpe_cmp_d (x, 17.0 / 42.0))
    printf ("dpe_div failed!\n"), abort ();

  /* test mul_ui */
  dpe_set_ui (x, 17);
  dpe_mul_ui (x, x, 42);
  if (dpe_cmp_ui (x, 714))
    printf ("dpe_mul_ui failed!\n"), abort ();

  /* test div_ui */
  dpe_set_ui (x, 17);
  dpe_div_ui (x, x, 42);
  if (dpe_cmp_d (x, 17.0 / 42.0))
    printf ("dpe_div_ui failed!\n"), abort ();

  /* test mul_2exp */
  dpe_set_ui (x, 17);
  dpe_mul_2exp (x, x, 1);
  if (dpe_cmp_ui (x, 34))
    printf ("dpe_mul_2exp failed!\n"), abort ();

  /* test div_2exp */
  dpe_set_ui (x, 17);
  dpe_div_2exp (x, x, 1);
  if (dpe_cmp_d (x, 8.5))
    printf ("dpe_div_2exp failed!\n"), abort ();

  /* test get_si_exp */
  dpe_set_d (x, 2147483647.0); /* 2^31-1 */
  e = dpe_get_si_exp (&l, x);
  dpe_set_d (y, ldexp ((double) l, e));
  if (dpe_cmp (x, y))
    printf ("dpe_get_si_exp failed!\n"), abort ();
  dpe_set_d (x, 18446744073709551616.0); /* 2^64 */
  e = dpe_get_si_exp (&l, x);
  dpe_set_d (y, ldexp ((double) l, e));
  if (dpe_cmp (x, y))
    printf ("dpe_get_si_exp failed!\n"), abort ();

  dpe_set_d (y, 17.0);
  e = dpe_get_si_exp (&l, y);
  dpe_set_si (x, l);
  if (e >= 0)
    dpe_mul_2exp (x, x, e);
  else
    dpe_div_2exp (x, x, -e);
  if (dpe_cmp (x, y))
    abort ();

  /* test out_str */
  dpe_out_str (stdout, 10, x);
  printf (" should be 17.\n");

  dpe_set_d (x, 1.5);
  dpe_out_str (stdout, 10, x);
  printf (" should be 1.5.\n");

  /* inp_str cannot be tested since we can't control stdin */

  /* test zero_p */
  dpe_set_ui (x, 0);
  if (dpe_zero_p (x) == 0)
    printf ("dpe_zero_p failed!\n"), abort ();
  dpe_set_ui (x, 1);
  if (dpe_zero_p (x) != 0)
    printf ("dpe_zero_p failed!\n"), abort ();

  /* test cmp */
  dpe_set_ui (x, 17);
  dpe_set_si (y, -42);
  if (dpe_cmp (x, x) != 0)
    printf ("dpe_cmp failed (1)\n"), abort ();
  if (dpe_cmp (x, y) <= 0)
    printf ("dpe_cmp failed (2)\n"), abort ();
  if (dpe_cmp (y, x) >= 0)
    printf ("dpe_cmp failed (3)\n"), abort ();

  /* test cmp_d */
  dpe_set_ui (x, 17);
  if (dpe_cmp_d (x, 17.0) != 0)
    abort ();
  if (dpe_cmp_d (x, -42.0) <= 0)
    abort ();
  if (dpe_cmp_d (x, 18.0) >= 0)
    abort ();

  /* test cmp_ui */
  dpe_set_ui (x, 17);
  if (dpe_cmp_ui (x, 17) != 0)
    abort ();
  if (dpe_cmp_ui (x, 16) <= 0)
    abort ();
  if (dpe_cmp_ui (x, 18) >= 0)
    abort ();

  /* test cmp_si */
  dpe_set_ui (x, 17);
  if (dpe_cmp_si (x, 17) != 0)
    abort ();
  if (dpe_cmp_si (x, -42) <= 0)
    abort ();
  if (dpe_cmp_si (x, 18) >= 0)
    abort ();

  /* test round */
  dpe_set_d (x, 17.49);
  dpe_round (x, x);
  if (dpe_cmp_ui (x, 17))
    abort ();
  dpe_set_d (x, 16.51);
  dpe_round (x, x);
  if (dpe_cmp_ui (x, 17))
    abort ();

  /* test frac */
  dpe_set_d (x, 17.49);
  dpe_frac (y, x);
  /* warning: binary64(0.49) differs from binary64(17.49) - 17 */
  if (dpe_get_d (y) != 17.49 - 17.0)
    printf ("dpe_frac failed!\n"), abort ();
  dpe_set_d (x, -17.49);
  dpe_frac (y, x);
  if (dpe_get_d (y) != -17.49 + 17.0)
    printf ("dpe_frac failed!\n"), abort ();
  dpe_set_d (x, 17.0);
  dpe_frac (y, x);
  if (dpe_get_d (y) != 0.0)
    printf ("dpe_frac failed!\n"), abort ();

  /* test floor */
  dpe_set_d (x, 17.49);
  dpe_floor (x, x);
  if (dpe_cmp_ui (x, 17))
    abort ();
  dpe_set_d (x, 16.51);
  dpe_floor (x, x);
  if (dpe_cmp_ui (x, 16))
    abort ();

  /* test ceil */
  dpe_set_d (x, 17.49);
  dpe_ceil (x, x);
  if (dpe_cmp_ui (x, 18))
    abort ();
  dpe_set_d (x, 16.51);
  dpe_ceil (x, x);
  if (dpe_cmp_ui (x, 17))
    abort ();

  /* check NaN */
  dpe_set_d (x, 0.0/0.0);
  dpe_out_str (stdout, 10, x);
  printf (" should be NaN.\n");

  /* check +Inf */
  dpe_set_d (x, 1.0/0.0);
  dpe_out_str (stdout, 10, x);
  printf (" should be +Inf.\n");

  /* check -Inf */
  dpe_set_d (x, -1.0/0.0);
  dpe_out_str (stdout, 10, x);
  printf (" should be -Inf.\n");

  dpe_clear (x);
  dpe_clear (y);
  mpz_clear (z);

  return 0;
}
